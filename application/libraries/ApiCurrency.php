<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiCurrency {

	public function getRatesFor($currencyFrom = "EUR", $currencyTo, $date = 'today')
	{
		$convert_q = "";
		if(is_array($currencyTo))
		{
			foreach ($currencyTo as $key => $value) 
			{
				$convert_q .= $currencyFrom."_".htmlspecialchars($value,ENT_QUOTES).",";
			}
			$convert_q = substr($convert_q,0,-1);
		}

		$convert_date = date("Y-m-d",strtotime("now"));
		if(strtotime($date))
		{
			$convert_date = date("Y-m-d",strtotime($date));
		}
		
		$CI =& get_instance();
		$CI->load->library('CurlProcess.php');
		return $CI->curlprocess->getData("https://free.currconv.com/api/v7/convert?q=".$convert_q."&compact=ultra&date=".$convert_date."&apiKey=".APIKEYCURRCONV);
	}

	public function getCyrrencyListing()
	{
		$CI =& get_instance();
		$CI->load->library('CurlProcess.php');
		return $CI->curlprocess->getData("https://free.currconv.com/api/v7/currencies?apiKey=".APIKEYCURRCONV);
	}
}