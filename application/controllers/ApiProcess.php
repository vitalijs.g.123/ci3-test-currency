<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiProcess extends CI_Controller {
	public function getcurrency($date)
	{
		if(isset($_POST['currencyFrom']) && isset($_POST['currencyTo']))
		{
			$this->load->library('ApiCurrency.php');
			//currencyFrom: "EUR", currencyTo: currencyToArray
			print $this->apicurrency->getRatesFor(htmlspecialchars($_POST['currencyFrom'],ENT_QUOTES),$_POST['currencyTo'],$date);
		}

	}

	public function apicurrencylist()
	{
		$this->load->library('ApiCurrency.php');
		print $this->apicurrency->getCyrrencyListing();
	}
}
