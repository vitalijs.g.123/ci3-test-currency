<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Currency Converter</title>

	<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#date" ).datepicker();
	$( "#date" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
	$( "#date" ).datepicker( "setDate", new Date());
  } );
  </script>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	2) attēlo valūtu sarakstu uz konkrēto dienu: <br>
	//ja domats VALUTU nosaukumi izvadit:<br>
	<button id="showCurrencyNames">Paradit Valutu nosaukumus</button><br>
		<select id="currencyNamesData"><option>Nospiediet pogu augstak</option></select>
	<br>
	2) attēlo valūtu sarakstu uz konkrēto dienu: <br>
	//ja domats VALUTU kursu izvadit (Bet Ta ka FREE versija limiteta radit tikai 2 variantus tad tikai 2)<br>
	//EUR kurs uz izveleto datumu pret USD un RUB<br>
	<button id="showCurrencyEURUR">Paradit Valutu kursu</button><input type="text" id="date">*jaizvelas datums<br>
	<div id="showCurrencyEURURData"></div>
	<br>
	<hr>
	3) Izveidot valūtu kalkulatoru, ar valūtu izvēli no kuras uz kuru valūtu aprēķināt.<br>
	<a href="/currencycalculator">Atvert Valutu kalkulatoru</a>
</div>

<script>
$(document).ready(function(){

	$("#showCurrencyNames").click(function(){
	$.get( "/apicurrencylist", function( data ) {
			$('#currencyNamesData').empty();
			$('#currencyNamesData'). empty(). append('<option>Nospiediet pogu augstak</option>');
			$.each(jQuery.parseJSON(data).results, function(key, item) 
			{
				console.log(item.id + " -- " + item.currencyName);
				$('#currencyNamesData').append($('<option>').val(item.id).text(item.id + " | " + item.currencyName))
			});
		});
		
		//
  });

  $("#showCurrencyEURUR").click(function(){
	var currencyToArray = new Array();
	currencyToArray.push("USD");
	currencyToArray.push("RUB");
	$('#showCurrencyEURURData').empty();

	$.post( "/apiprocess/" + $("#date").val(), { currencyFrom: "EUR", currencyTo: currencyToArray})
  	.done(function( data ) {
		$.each(jQuery.parseJSON(data), function(key, item) 
			{
				console.log(key + " --");
				$('#showCurrencyEURURData').append(key + "<br>");
				$.each(item, function(keydate, itemdate) 
				{
					console.log(keydate + " -- " + itemdate);
					$('#showCurrencyEURURData').append(keydate + " -- " + itemdate + "<br>");
				});
			});
  	});
  });  
});
</script>

</body>
</html>